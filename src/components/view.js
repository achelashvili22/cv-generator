export default {
    template: `
        <!-- View -->
        <div class='view-main'>

            <div class='view-general'>

                <label v-if="$parent.$parent.general.name!=null" class='view-name'>{{ $parent.$parent.general.name }}</label>
                <label v-if="$parent.$parent.general.surname!=null" class='view-surname'>{{ $parent.$parent.general.surname }}</label>

                <img v-if="$parent.$parent.picture.uploaded!=false" class='view-picture' :src='$parent.$parent.picture.blob'/>

                <img v-if="$parent.$parent.general.email!=null" class='view-mail' src='/src/img/mail.svg'/>
                <label v-if="$parent.$parent.general.email!=null" class='view-email-text'>{{ $parent.$parent.general.email }}</label>

                <img v-if="$parent.$parent.general.phone!=null" class='view-phone' src='/src/img/phone.svg'/>
                <label v-if="$parent.$parent.general.phone!=null" class='view-phone-text'>{{ $parent.$parent.general.phone }}</label>

                <label v-if="$parent.$parent.general.about!=null" class='view-label-aboutme'>ჩემ შესახებ</label>

                <label v-if="$parent.$parent.general.about!=null" class='view-aboutme'>{{ $parent.$parent.general.about }}</label>

            </div>

            <div class='view-experience' v-if="this.$route.path!='/general'">

                <div class='view-hr'></div>

                <label class='view-lab-experience'>გამოცდილება</label>

                <table v-for='field in $parent.$parent.experience' class='view-exp-table'>
                    <tr>
                        <td>
                            <label v-if="field.position!=null" class='view-tbl-td1'>{{ field.position }}, {{ field.employer }}</label><br>
                            <label v-if="field.start_date!=null" class='view-tbl-td2'>{{ $parent.$parent.formatDate(field.start_date) }} - {{ $parent.$parent.formatDate(field.end_date) }}</label><br>
                            <label class='view-tbl-td3'>{{ field.info }}</label>
                        </td>
                    </tr>
                </table>
            </div>

            

            <div class='view-experience' v-if="this.$route.path!='/general' || this.$route.path!='/education'">

                <div class='view-hr'></div>

                <label class='view-lab-experience'>განათლება</label>

                <table v-for='field in $parent.$parent.education' class='view-exp-table'>
                    <tr>
                        <td>
                            <label v-if="field.univer!=null" class='view-tbl-td1'>{{ field.univer }}, {{ degrees[field.degree-1]!==undefined ? degrees[field.degree-1]['title'] : null }}</label><br>
                            <label v-if="field.end_date!=null" class='view-tbl-td2'>{{ $parent.$parent.formatDate(field.end_date) }}</label><br>
                            <label class='view-tbl-td3'>{{ field.info }}</label>
                        </td>
                    </tr>
                </table>
            </div>

        </div>`
  }