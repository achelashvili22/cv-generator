import View from '../components/view.js'

export default {
    data() {
      return {
        validation: [],
        degrees: []
      }
    },
    components: {
        Datepicker: VueDatePicker
    },
    methods: {
        //restart CV Generator
        restart() {
          this.$parent.$parent.startOver();
        },
        renderValidationClass(idx,key){
            if(this.validation.length>0)
                if(this.validation[idx][key]==false)
                    return 'invalid';
                else if(this.validation[idx][key]==true)
                    return 'valid';
        },

        getDegrees(){
            let app = this;
            axios.get('https://resume.redberryinternship.ge/api/degrees').then(resp => {
                app.degrees = resp.data;
            });
        },

        addEducation(){
            this.$parent.$parent.education.push({ univer: null,degree: null,end_date: null,info: null });
            this.initValidationArray();
        },

        //go back
        goBack(){
            this.$parent.$parent.backToExperience();
        },

        //init
        initValidationArray(){
            const app = this;
            app.getDegrees();

            const appEducation = app.$parent.$parent.education;
            appEducation.forEach(el => {
                app.validation.push({ univer: el.univer,degree: el.degree, end_date: el.end_date, info: el.info });
            });

            
        },

        renderSVGs(index,key){
            if(this.validation.length!=0)
                if(this.validation[index][key]==true)
                    return "<img class='svg-valid-full' src='/src/img/valid.svg'/>";
                else if(this.validation[index][key]==false)
                    return "<img class='svg-invalid' src='/src/img/invalid.svg'/>";
        },

        renderTextareaClass(item){
            if(item.info==null)
              return 'form-textarea';
            else if(item.info==false)
              return 'form-textarea input-invalid';
            else
              return 'form-textarea input-valid';
          },

        //dynamic data update
        update(index,key){

            let educationVariable = this.$parent.$parent.education;

            //validate each
            if(key=='univer')
                if(educationVariable[index].univer.length<3){
                    this.validation[index].univer=false;
                    return;
                }else{
                    this.validation[index].univer=true;
                }
            else
                if(educationVariable[index][key].length<3){
                    this.validation[index][key]=false;
                    return;
                }else{
                    this.validation[index][key]=true;
                }
                
            this.$parent.$parent.updateEducation();
        },

        finishForm(){
            let formValid = true;

            Object.values(this.validation).forEach(el => {
                Object.values(el).forEach(item => {
                  if(item==null || item==false)
                      formValid=false;
                });
            });
    
            if(formValid==false){
              Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: 'დაფიქსირდა შეცდომა',
                showConfirmButton: false,
                timer: 2500
              });
              return;
            }

            this.sendForm();
        },

        //code from https://gist.github.com/Klerith/e22c546d433226c47cedb4307846bb64
        dataURItoBlob(dataURI) {

            var byteString = atob(dataURI.split(',')[1]);
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
            
            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);
            
            for (var i = 0; i < byteString.length; i++) {
              ia[i] = byteString.charCodeAt(i);
            }
            var blob = new Blob([ab], {type: mimeString});
            
            return blob;
        },

        sendForm(){
            let base = this.$parent.$parent;
            const formData = new FormData();

            base.updateGeneralInfoValues();
            base.retrieveExperience();
            
            formData.append('name',base.general.name);
            formData.append('surname',base.general.surname);
            formData.append('email',base.general.email);
            formData.append('phone_number',base.general.phone.replace(/\s/g, ""));
            formData.append('about_me',base.general.about);

            let experiences = new Array(); 
            Object.entries(base.experience).forEach(exp => {
                experiences.push({
                    'position':exp[1].position,
                    'employer':exp[1].employer,
                    'start_date':base.postFormat(exp[1].start_date),
                    'due_date':base.postFormat(exp[1].end_date),
                    'description':exp[1].about
                });
            });

            let educations = new Array();
            Object.entries(base.education).forEach(exp => {
                educations.push({
                    'institute':exp[1].univer,
                    'degree_id':exp[1].degree,
                    'due_date':base.postFormat(exp[1].end_date),
                    'description':exp[1].about
                })
            });

            formData.append('experiences[]', JSON.stringify(experiences)); //send as array
            formData.append('educations[]', JSON.stringify(educations)); //send as array

            const blob = this.dataURItoBlob(base.picture.blob);
            formData.append('image', blob);

            //formdata foreach
            formData.forEach((value, key) => {
                console.log(key + ': ' + value);
            });

            axios
              .post("https://resume.redberryinternship.ge/api/cvs", formData, {
                headers: {
                  "Content-Type": "multipart/form-data",
                },
              })
              .then((res) => {
                console.log(res);
              })
              .catch((err) => {
                console.log(err.response.data);
              });

            /** Unmeintenable */

            return;
        }
    },
    mounted(){
        this.$parent.$parent.retrieveGeneralInfo();
        this.$parent.$parent.retrieveExperience();
        this.$parent.$parent.retrieveEducation();
        this.initValidationArray();
    },
    template: `
    <div>
    
        <div class='gn-edit'>

            <!-- BREADCUMP -->
            <div class='breadcump-items'>
                <div @click="restart" class='goBack'>
                    <img class='ellipse' src='/src/img/ellipse.svg'/>
                    <img class='arrow-left' src='/src/img/arrow-left.svg'/>
                </div>
                <label class='breadcump-info'>განათლება</label>
                <label class='breadcump-number'>3/3</label>
                <div class='breadcump-hr'></div>
            </div>

            <!-- EDIT FORM -->
            <div class='edit-form'>

                <div class='form-full-group' v-for='item,idx in $parent.$parent.education'>

                    <div class='form-full-row'>

                        <label for='input_position' :class="'form_input_label label-'+renderValidationClass(idx,'univer')">სასწავლებელი</label>
                        <input type='text' :class="'form-input-full input-'+renderValidationClass(idx,'univer')" placeholder='სასწავლებელი' v-model='item.univer' @change="update(idx,'univer')"/>

                        <label class='form-full-hint'>მინიმუმ ორი სიმბოლო</label>   
                        <div class='svg-div-exp' v-html="renderSVGs(idx,'univer')"></div>

                    </div>

                    <!-- Dropdown -->

                    <div class='form-group-left'>

                        <label class='form_input_label'>ხარისხი</label>

                        <select class='form_input_right ed-select' v-model='item.degree' name='degree' @change="update(idx,'degree')">
                            <option :value=dgr.id class='ed-option' v-for='dgr in degrees' :selected="item.degree==dgr.id">{{ dgr.title }}</option>
                        </select>

                    </div>

                    <div class='form-group-right'>

                        <label class='form_input_label'>დამთავრების რიცხვი</label>

                        <Datepicker class='form_input_right' v-model="item.end_date" :is-24="false" show-now-button now-button-label="დღეს" select-text="არჩევა" month-name-format="short" :enable-time-picker="false" @update:model-value="update(idx,'end_date')"/>

                    </div>

                    <div class='form-full-row'>
                    
                        <label :class="'textarea-lab label-'+renderValidationClass(idx,'info')">აღწერა</label>
                        <textarea :class="renderTextareaClass(item)" placeholder='როლი თანამდებობაზე და ზოგადი აღწერა' v-model='item.info' @change="update(idx,'info')"></textarea>

                    </div>

                    <div class='exp-hr'></div>

                </div>

                <div class='form-full-row'>
                    <button type='button' class='btn-add-experience' @click='addEducation'>მეტი სასწავლებლის დამატება</button>
                </div>

                <button type='button' class='btn-goback' @click='goBack'>უკან</button>  
                <button type='button' class='btn-submit-fin' @click='finishForm'>დასრულება</button>
            </div>

        </div>

        `+View.template+`
    </div>`
}