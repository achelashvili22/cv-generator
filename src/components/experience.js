import View from '../components/view.js'

export default {
    data() {
      return {
        validation: []
      }
    },
    components: {
        Datepicker: VueDatePicker
    },
    methods: {
      //restart CV Generator
      restart() {
        this.$parent.$parent.startOver();
      },
      //go back
      goBack(){
        this.$parent.$parent.backToGeneral();
      },

      //init
      initValidationArray(){
          const app = this;
          const appExperience = app.$parent.$parent.experience;

          appExperience.forEach(el => {
              app.validation.push({ position: el.position,employer: el.employer, start_date: el.start_date, end_date: el.end_date, info: el.info });
          });
      },

      /** Experience */

      addExperience(){
        this.$parent.$parent.experience.push({ position: null,employer: null,start_date: null,end_date: null,info: null });
        this.initValidationArray();
      },

      renderValidationClass(idx,key){
        if(this.validation.length>0)
          if(this.validation[idx][key]==false)
            return 'invalid';
          else if(this.validation[idx][key]==true)
            return 'valid';
      },

      renderSVGs(index,key){
        if(this.validation.length!=0)
          if(this.validation[index][key]==true)
            return "<img class='svg-valid-full' src='/src/img/valid.svg'/>";
          else if(this.validation[index][key]==false)
            return "<img class='svg-invalid' src='/src/img/invalid.svg'/>";
      },

      renderTextareaClass(item){
        if(item.info==null)
          return 'form-textarea';
        else if(item.info==false)
          return 'form-textarea input-invalid';
        else
          return 'form-textarea input-valid';
      },

      //dynamic data update
      update(index,key){

        let parent = this.$parent.$parent;
        let experienceVariable = this.$parent.$parent.experience;

        if(key.includes('date'))
          if(String(experienceVariable[index][key]).length>0){
            experienceVariable[index][key] = parent.formatDate(experienceVariable[index][key]);
            this.validation[index][key]=true;
          }
          else{
            this.validation[index][key]=false;
            return;
          }
        else
          if(experienceVariable[index][key].length<3){
            this.validation[index][key]=false;
            return;
          }else{
            this.validation[index][key]=true;
          }

        this.$parent.$parent.updateExperience();
      },

      finishForm(){

        let formValid = true;

        Object.values(this.validation).forEach(el => {
            Object.values(el).forEach(item => {
              if(item==null || item==false)
                  formValid=false;
            });
        });

        if(formValid==false){
          Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: 'დაფიქსირდა შეცდომა',
            showConfirmButton: false,
            timer: 2500
          });
          return;
        }

        this.$parent.$parent.goToEducation();
      }
    },
    mounted(){
        this.$parent.$parent.retrieveGeneralInfo();
        this.$parent.$parent.retrieveExperience();
        this.initValidationArray();
    },
    template: `<div>
    
      <div class='gn-edit'>

          <!-- BREADCUMP -->
          <div class='breadcump-items'>
            <div @click="restart" class='goBack'>
              <img class='ellipse' src='/src/img/ellipse.svg'/>
              <img class='arrow-left' src='/src/img/arrow-left.svg'/>
            </div>
            <label class='breadcump-info'>გამოცდილება</label>
            <label class='breadcump-number'>2/3</label>
            <div class='breadcump-hr'></div>
          </div>

          <!-- EDIT FORM -->
          <div class='edit-form'>

              <div class='form-full-group' v-for='item,idx in $parent.$parent.experience'>

                    <div class='form-full-row'>

                        <label for='input_position' :class="'form_input_label label-'+renderValidationClass(idx,'position')">თანამდებობა</label>
                        <input type='text' :class="'form-input-full input-'+renderValidationClass(idx,'position')" placeholder='თანამდებობა' v-model='item.position' @change="update(idx,'position')"/>

                        <label class='form-full-hint'>მინიმუმ ორი სიმბოლო</label>   
                        <div class='svg-div-exp' v-html="renderSVGs(idx,'position')"></div>

                    </div>

                    <div class='form-full-row-append'>

                        <label for='input_employer' class='form-full-label'>დამსაქმებელი</label>
                        <input type='text' :class="'form-input-full input-'+renderValidationClass(idx,'employer')" placeholder='დამსაქმებელი' v-model='item.employer' @change="update(idx,'employer')"/>

                        <label class='form-full-hint'>მინიმუმ ორი სიმბოლო</label>   
                        <div class='svg-div-exp' v-html="renderSVGs(idx,'employer')"></div>

                    </div>





                    <!-- Dates -->

                    <div class='form-group-left'>

                        <label class='form_input_label'>დაწყების რიცხვი</label>

                        <Datepicker class='form_input_right' v-model="item.start_date" :is-24="false" show-now-button now-button-label="დღეს" select-text="არჩევა" month-name-format="short" :enable-time-picker="false" @update:model-value="update(idx,'start_date')"/>

                    </div>

                    <div class='form-group-right'>

                        <label class='form_input_label'>დამთავრების რიცხვი</label>

                        <Datepicker class='form_input_right' v-model="item.end_date" :is-24="false" show-now-button now-button-label="დღეს" select-text="არჩევა" month-name-format="short" :enable-time-picker="false" @update:model-value="update(idx,'end_date')"/>

                    </div>



                    <div class='form-full-row' v-if="validation[idx]!==undefined">
                    
                        <label :class="'textarea-lab label-'+renderValidationClass(idx,'info')">აღწერა</label>
                        <textarea :class="renderTextareaClass(item)"  placeholder='როლი თანამდებობაზე და ზოგადი აღწერა' v-model='item.info' @change="update(idx,'info')"></textarea>

                    </div>

                    <div class='exp-hr'></div>

                    

                    
              </div>

              <div class='form-full-row'>
                  <button type='button' class='btn-add-experience' @click='addExperience'>მეტი გამოცდილების დამატება</button>
              </div>

              <button type='button' class='btn-goback' @click='goBack'>უკან</button>  
              <button type='button' class='btn-submit' @click='finishForm'>შემდეგი</button>

          </div>

      </div>

      `+View.template+`
    </div>`
  }