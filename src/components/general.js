import View from '../components/view.js'

export default {
  data() {
    return {
      validation:{
        name: null,
        surname: null,
        email: null,
        phone: null
      },
    }
  },
  methods: {

    //initialize validation array
    initValidationArray(){
      this.validation.name = this.$parent.$parent.general.name;
      this.validation.surname = this.$parent.$parent.general.surname;
      this.validation.email = this.$parent.$parent.general.email;
      this.validation.phone = this.$parent.$parent.general.phone;
    },

    //restart CV Generator
    restart() {
      this.$parent.$parent.startOver();
    },

    /** File Upload & Save (Jquery) */
    openFileUpload(){
        $("#form_upload_picture").trigger('click');
    },

    savePicture(){
      const parentClass = this.$parent.$parent;
      
      //image path
      const imgPath = this.$refs.file.files[0];
            parentClass.picture.file = imgPath;

      var freader1 = new FileReader();
          freader1.onload = function(event){
              parentClass.picture.blob = event.target.result;
          }
          freader1.readAsDataURL(imgPath);
     
      var freader2 = new FileReader();    
          freader2.onloadend = function() {
            sessionStorage.setItem('blob',freader2.result);
            
            parentClass.picture.uploaded=true;
          }
          freader2.readAsDataURL(imgPath);
    },

    /** Validation */
    isGeorgian(text){
      for (let i = 0; i < text.length; i++) {
        if(text.charCodeAt(i)<4304 || text.charCodeAt(i)>4336)
          return false;
      }

      return true;
    },

    validateData(target){
      const app = this;
      const targetName = target.name;
      const value = target.value;
      const emailRegex = /^[A-Za-z, 0-9,*!@#$%^&()-=+„“?<>":;']{13,}$/;

      //if empty value
      if(value==''){
        app.validation[targetName] = false;
        return false;
      }

      //name and surname
      if(targetName=='name' || targetName=='surname'){
        if(this.isGeorgian(value) && value.length>2){
          app.validation[targetName] = true;
          return true;
        }else{
          app.validation[targetName] = false;
          return false;
        }
      }

      //email
      if(targetName=='email'){
        if(value.endsWith("@redberry.ge") && emailRegex.test(value)){
          app.validation[targetName] = true;
          return true;
        }
        else{
          app.validation[targetName] = false;
          return false;
        }
      }

      //phone
      if(targetName=='phone'){

        const value = target.value;
        const number = value.replace(/\s/g, "");

        console.log(number)
        console.log(number.length)

        //main
        if(value.startsWith('+') && /^\d+$/.test(number.substring(1))){

          if(number.length<13 || number.length>13){
            app.validation[targetName] = false;
          }
          else{
            app.$parent.$parent.general.phone = (target.value).replace(/(\d{3})(\d{3})(\d{2})(\d{2})(\d{2})/, '$1 $2 $3 $4 $5');
            app.validation[targetName] = true;
            return true;
          }

        }

      }
      
      return false;    
    },

    renderValidationClass(name){
      if(this.validation[name]==false)
        return 'invalid';
      else if(this.validation[name]==true)
        return 'valid';
    },

    renderSVGs(name){
      if(this.validation[name]==true)
        if(name=='name' || name=='surname')
          return "<img class='svg-valid' src='/src/img/valid.svg'/>";
        else
          return "<img class='svg-valid-full' src='/src/img/valid.svg'/>";
      else if(this.validation[name]==false)
        return "<img class='svg-invalid' src='/src/img/invalid.svg'/>";
    },

    //update if VALID
    update(event){
        //skip about
        if(event.target.name=='about')
           this.$parent.$parent.updateGeneralInfoValues();

        else if(this.validateData(event.target))
           this.$parent.$parent.updateGeneralInfoValues();
    },

    //Finish Form
    finishForm(){

      let formValid = true;
      
      Object.values(this.validation).forEach(el => {
          if(el==null || el==false)
            formValid=false;
      });

      if(formValid==false){
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'დაფიქსირდა შეცდომა',
          showConfirmButton: false,
          timer: 2500
        });
        return;
      }
        
      this.$parent.$parent.goToExperience();
    }
  },
  mounted(){
      this.$parent.$parent.retrieveGeneralInfo();
      this.$parent.$parent.retrieveExperience();
      this.initValidationArray();
  },
  template: 
  `
  <div>
    <div class='gn-edit'>

        <!-- BREADCUMP -->
        <div class='breadcump-items'>
          <div @click="restart" class='goBack'>
            <img class='ellipse' src='/src/img/ellipse.svg'/>
            <img class='arrow-left' src='/src/img/arrow-left.svg'/>
          </div>
          <label class='breadcump-info'>პირადი ინფო</label>
          <label class='breadcump-number'>1/3</label>
          <div class='breadcump-hr'></div>
        </div>

        <!-- EDIT FORM -->
        <div class='edit-form'>

            <div class='form-group-left'>

                <label :class="'form_input_label label-'+renderValidationClass('name')">სახელი</label>
                <input :class="'form_input_left input-'+renderValidationClass('name')" name='name' placeholder='ანზორ' type='text' v-model='$parent.$parent.general.name' @change='update($event)'/>
                <label class='form-label-hint'>მინიმუმ 2 ასო, ქართული ასოები</label>
                <div class='svg-div' v-html="renderSVGs('name')"></div>
                
            </div>

            <div class='form-group-right'>

                <label :class="'form_input_label label-'+renderValidationClass('surname')">გვარი</label>
                <input :class="'form_input_left input-'+renderValidationClass('surname')" name='surname' placeholder='მუმლაძე' type='text' v-model='$parent.$parent.general.surname' @change='update($event)'/>
                <label class='form-label-hint'>მინიმუმ 2 ასო, ქართული ასოები</label>
                <div class='svg-div' v-html="renderSVGs('surname')"></div>

            </div>

            <div class='form-full-group'>

                <div class='form-upload'>
                    <label class='form-lab-upload'>პირადი ფოტოს ატვირთვა</label>
                    <input id="form_upload_picture" type="file" ref="file" @change='savePicture'/>
                    <span class='form-btn-upload' @click='openFileUpload()' >ატვირთვა</span>
                </div>

                <div class='form-full-row'>
                    
                    <label for='input_about' class='textarea-lab'>ჩემ შესახებ (არასავალდებულო)</label>
                    <textarea id='input_about' name='about' class='form-textarea' placeholder='ზოგადი ინფო შენ შესახებ' v-model='$parent.$parent.general.about' @change='update'></textarea>

                </div>

                <div class='form-full-row'>

                    <label :class="'form-full-label label-'+renderValidationClass('email')">ელ.ფოსტა</label>
                    <input type='email' :class="'form-input-full input-'+renderValidationClass('email')" name='email' placeholder='anzorr666@redberry.ge' v-model='$parent.$parent.general.email' @change='update'/>
                    <label class='form-full-hint'>უნდა მთავრდებოდეს @redberry.ge-ით</label>   
                    <div class='svg-div-full svg-div-full-svg' v-html="renderSVGs('email')"></div>

                </div>

                <div class='form-full-row'>

                    <label :class="'form-full-label label-'+renderValidationClass('phone')">მობილურის ნომერი</label>
                    <input type='text' :class="'form-input-full input-'+renderValidationClass('phone')" name='phone' placeholder='+995 551 12 34 56' v-model='$parent.$parent.general.phone' @change='update'/>
                    <label class='form-full-hint'>უნდა აკმაყოფილებდეს ქართული მობილურის ნომრის ფორმატს</label>   
                    <div class='svg-div-full svg-div-full-svg' v-html="renderSVGs('phone')"></div>

                </div>
                
            </div>

            <button type='button' class='btn-submit' @click='finishForm'>შემდეგი</button>

        </div>
    </div>
    `+View.template+`
  </div>
  `
  
}