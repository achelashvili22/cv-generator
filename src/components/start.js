export default {
    methods: {
     //start CV Generator
      start() {
        this.$parent.$parent.goToGeneral();
      }
    },
    template: `
      <div class='start-main'>
        <img class='start-header-logo' src="/src/img/LOGO-02.svg"/>

        <div class='start-hr'></div>

        <img class='start-stamp-logo' src='/src/img/LOGO-40.png'/>

        <button class='start-btn' @click='start'>
            <label class='start-btn-label'>რეზიუმეს დამატება</label>
        </button>
      </div>`
  }