/** Import Page Components */
import Start from '../components/start.js'
import General from '../components/general.js'
import Experience from '../components/experience.js'
import Education from '../components/education.js'

//pass components to Router
const routes = [
  { path: '/', component: Start },
  { path: '/general', component: General },
  { path: '/experience', component: Experience },
  { path: '/education', component: Education },
]

//init router
const router = VueRouter.createRouter({
    history: VueRouter.createWebHistory(),
    routes, 
})

var appMain = Vue.createApp({
    data() {
      return {

        //page1
        general: { name: null, surname: null, phone: null, email: null, about: null },
    
        //page2
        experience: [
            {   position: null,employer: null,start_date: null,end_date: null,info: null }
        ],

        //page3
        education: [
            {   univer: null,degree: null,end_date: null,info: null }
        ],
  
        //picture
        picture:{ uploaded: false, blob: null, file: null }
      }
    },
    components: {
        Start,
        General,
        Experience,
        Education
    },
    methods: {

        //reset Application
        resetApp(){

            this.general={   name: null,surname: null,phone: null,email: null,about: null },

            this.experience=[
                { position: null, employer: null, start_date: null, end_date: null, info: null}
            ],

            this.education=[
                { univer: null, degree: null, end_date: null, info: null}
            ],

            this.picture={  uploaded: false,blob: null,file: null}
        },

        //retrieve from sessionStorage
        retrieveGeneralInfo(){

            this.general.name = sessionStorage.getItem('general') ? ( JSON.parse(sessionStorage.getItem('general'))['name'] ?? null ):null;
            this.general.surname = sessionStorage.getItem('general') ? ( JSON.parse(sessionStorage.getItem('general'))['surname'] ?? null ):null;
            this.general.about = sessionStorage.getItem('general') ? ( JSON.parse(sessionStorage.getItem('general'))['about'] ?? null ):null;
            this.general.phone = sessionStorage.getItem('general') ? ( JSON.parse(sessionStorage.getItem('general'))['phone'] ?? null ):null;
            this.general.email = sessionStorage.getItem('general') ? ( JSON.parse(sessionStorage.getItem('general'))['email'] ?? null ):null;

            //picture if available
            if(sessionStorage.getItem('blob')){
                this.picture.uploaded=true;
                this.picture.blob = sessionStorage.getItem('blob');
            }

            this.$forceUpdate()
        },

        //update general info
        updateGeneralInfoValues(){

            sessionStorage.setItem('general',
                JSON.stringify({
                'name':this.general.name,
                'surname':this.general.surname,
                'about':this.general.about,
                'phone':this.general.phone,
                'email':this.general.email
                })
            );
            this.$forceUpdate()
        },

        //add new Experience
        addBlankExperience(){
            this.experience.push({
                position: null,
                employer: null,
                start_date: null,
                end_date: null,
                info: null
            });
        },

        updateExperience(){
            sessionStorage.setItem('experience',
                JSON.stringify(this.experience)
            );
            this.$forceUpdate()
        },

        updateEducation(){
            sessionStorage.setItem('education',
                JSON.stringify(this.education)
            );
            this.$forceUpdate()
        },

        //retrieve from sessionStorage
        retrieveExperience(){
            const sessionData = sessionStorage.getItem('experience');
            const json = JSON.parse(sessionData);

            if(json!==null)
                this.experience = json;
        },

        //retrieve from sessionStorage
        retrieveEducation(){
            const sessionData = sessionStorage.getItem('education');
            const json = JSON.parse(sessionData);
            
            if(json!==null)
                this.education = json;
        },

        //momentjs
        formatDate(date){
            if(date!==null)
                return moment(new Date(date)).format("MM/DD/YYYY");
        },

        //format date for POST
        postFormat(date){
            if(date!==null)
                return moment(new Date(date)).format("YYYY/MM/DD");
        },

        /** Location Changes */
        startOver(){
            sessionStorage.clear();
            this.resetApp();
            router.replace({ path: '/' })
        },
        backToGeneral(){
            router.replace({ path: '/general' })
        },
        backToExperience(){
            router.replace({ path: '/experience' })
        },
        //with data clear
        goToGeneral(){
            sessionStorage.clear();
            this.resetApp();
            router.replace({ path: '/general' })
        },
        goToExperience(){
            router.replace({ path: '/experience' })
        },
        goToEducation(){
            router.replace({ path: '/education' })
        },
    }   
    ,mounted(){}
});

appMain.use(router);
appMain.mount('#main')